<?php

class Home extends Controller
{
	public function index()
	{
		// Anggap aja middleware
		redirect_unless((new Auth())->hasUser(), url('home/login'));

		$data['title'] = 'Home';

		$data['users'] = $this->model('User')->all('id, username, email, first_name, last_name');

		if (count($data['users'])) {
			$data['table_users_columns'] = array_keys($data['users'][0]);
		}

		$this->view('templates/header', $data);
		$this->view('home/index', $data);
		$this->view('templates/footer', $data);
	}

	public function about($company = 'SMKN1')
	{
		// Anggap aja middleware
		redirect_unless((new Auth())->hasUser(), url('home/login'));

		$data['title'] = 'About';
		$data['company'] = $company;

		$this->view('templates/header', $data);
		$this->view('home/about', $data);
		$this->view('templates/footer', $data);
	}

	public function profile()
	{
		redirect_unless((new Auth())->hasUser(), url('home/login'));

		$data['title'] = 'Profile';
		$data['user'] = (new Auth())->user();

		$this->view('templates/header', $data);
		$this->view('home/profile', $data);
		$this->view('templates/footer', $data);
	}

	public function register()
	{
		// Anggap aja middleware
		redirect_if((new Auth())->hasUser(), url('home/index'));

		$data['title'] = 'Register';

		$this->view('auth/register', $data);
	}

	public function store()
	{
		// Anggap aja middleware
		redirect_if((new Auth())->hasUser(), url('home/index'));

		$data = [
			'email' => $_POST['email'],
			'username' => $_POST['username'],
			'first_name' => $_POST['first-name'],
			'last_name' => $_POST['last-name'],
			'password' => password_hash($_POST['password'], PASSWORD_BCRYPT),
		];

		return $this->model('User')->create($data) ?
			redirect(url('home/login'), true) :
			back(['login-failed' => 'Login gagal']);
	}

	public function login($data = [])
	{
		// Anggap aja middleware
		redirect_if((new Auth())->hasUser(), url('home/index'));

		$data['title'] = 'Login';

		$this->view('auth/login', $data);
	}

	public function authenticate()
	{
		// Anggap aja middleware
		redirect_if((new Auth())->hasUser(), url('home/index'));

		$credentials = [
			'email' => $_POST['email'],
			'password' => $_POST['password'],
		];

		return (new Auth())->attempt($credentials) ?
			redirect(url('home/index'), true) : back();
	}

	public function logout()
	{
		(new Auth())->logout();

		return back();
	}
}
