<?php

require_once('config/config.php');

require_once('core/Interface/AuthInterface.php');
require_once('core/Interface/DatabaseInterface.php');
require_once('core/Interface/SessionInterface.php');

require_once('core/Helper/Array.php');
require_once('core/Helper/String.php');

require_once('core/App.php');
require_once('core/Database.php');
require_once('core/Session.php');
require_once('core/Auth.php');

require_once('core/Controller.php');
require_once('core/Model.php');

require_once('core/Helper.php');
