<?php

class App
{
	protected $controller = "Home";
	protected $method = "index";
	protected $params = [];

	public function __construct()
	{
		$url = $this->parseUrl();

		$class = ucwords($url[0]);

		if (file_exists("../app/controller/$class.php")) {
			$this->controller = $class;
			require_once("../app/controller/$class.php");

			unset($url[0]);
		} else {
			//require_once('../app/controller/Home.php');

			return $this->notFoundView();
		}

		$this->controller = new $this->controller;

		$method = $url[1];

		if (method_exists($this->controller, $method)) {
			$this->method = $method;
			unset($url[1]);
		} else {
			return $this->notFoundView();
		}

		$this->params = array_values($url);

		call_user_func_array(
			[$this->controller, $this->method],
			$this->params
		);
	}

	private function parseUrl()
	{
		if ($_GET['url']) {
			$url = $_GET['url'];

			if (str_contains($url, 'public'))
				$url = str_replace('public/', '', $url);

			$url = rtrim($url, '/');

			$url = filter_var($url, FILTER_SANITIZE_URL);

			$urls = explode('/', $url);

			return $urls;
		}
	}

	private function notFoundView()
	{
		echo "404 | Not Found";
	}
};
