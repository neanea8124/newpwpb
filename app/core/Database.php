<?php

use App\Core\Interface\DatabaseInterface;

class Database implements DatabaseInterface
{
	private $host, $port, $user, $password, $database;

	private $dsn, $options;

	private $connection, $statement;

	/**
	 * 
	 */
	public function __construct()
	{
		$this->host = DB_HOST;
		$this->port = DB_PORT;
		$this->user = DB_USER;
		$this->password = DB_PASS;
		$this->database = DB_NAME;

		$this->dsn = "mysql:host={$this->host}:{$this->port};dbname={$this->database}";
		$this->options = [
			//PDO::ATTR_PERSISTENT => true,
			PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
		];

		$this->connect();
	}

	/**
	 * @inheritDoc
	 */
	public function connect()
	{
		try {
			$this->connection = new PDO(
				$this->dsn,
				$this->user,
				$this->password,
				$this->options
			);

			return $this;
		} catch (PDOException $e) {
			die($e->getMessage());
		}
	}

	/**
	 * @inheritDoc
	 */
	public function query($query)
	{
		$this->statement = $this->connection->prepare($query);

		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function bind($param, $value, $type = null)
	{
		if (is_null($type)) {
			switch (1) {
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;
				case is_bool($value):
					$type = PDO::PARAM_BOOL;
					break;
				case is_null($value):
					$type = PDO::PARAM_NULL;
					break;
				default:
					$type = PDO::PARAM_STR;
			}
		}

		$this->statement->bindValue($param, $value, $type);

		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function bindAll($bindings)
	{
		foreach ($bindings as $param => $value) {
			$this->bind($param, $value);
		}

		return $this;
	}

	/**
	 * @inheritDoc
	 */
	public function execute()
	{
		try {
			$this->statement->execute();

			return $this;
		} catch (Throwable $e) {
			dd($e->getMessage());
			return null;
		}
	}

	/**
	 * @inheritDoc
	 */
	public function result()
	{
		$this->execute();

		return $this->statement->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * @inheritDoc
	 */
	public function results()
	{
		$this->execute();

		return $this->statement->fetchAll(PDO::FETCH_ASSOC);
	}
}
